# Dizzydata API Client

This package has been replaced by the [dizzydata](https://www.npmjs.com/package/dizzydata "dizzydata package homepage") package.

Install that package instead.